-----------------------------------------------------------------
--  Simple Key Account Entries by cust_name string search
--  Central TX uses addr
--  UNFI uses cust_name ilike 'UNFI Sales'
--  Author: Diana Newcomb
-----------------------------------------------------------------

WITH simple
(group_name, cust_name, cust_number,
cust_id)

AS (
 SELECT DISTINCT
 CASE WHEN (cust_name ilike 'central market%'
    AND addr_state ilike 'TX') THEN 'Central Market - TX'
    WHEN (cust_name ilike 'Akin''s Natural Foods%' OR cust_name ilike 'Chamberlin''s Natural Foods%' OR cust_name ilike '%earth%Origin%')
        THEN 'Akins-Chamberlins-EO'
    WHEN cust_name ilike 'fresh thyme farmers market%' THEN 'Fresh Thyme'
    WHEN cust_name ilike 'green acres%' THEN 'Green Acres'
    WHEN cust_name ilike '%market street #%' THEN 'Market Street'
    WHEN cust_name ilike 'pcc%' THEN 'PCC'
    when cust_name ilike 'Mom''s Organic Market%' THEN 'Mom''s Market'
    when cust_name ilike 'mother''s market & kitchen%' THEN 'Mother''s Market'
    when (cust_name ilike 'new leaf community market%' OR cust_name ilike 'new seasons market%') THEN 'New Leaf - New Seasons'
    when cust_name ilike 'pharmaca%' THEN 'Pharmaca'
    when cust_name ilike 'sprouts farmers market%' THEN 'Sprouts'
    when cust_name ilike 'vitamin cottage%' THEN 'Vitamin Cottage'
    when (cust_name ilike 'whole foods market%' OR cust_name ilike '365%' or cust_name ilike '%UNFI%') THEN 'UNFI Sales'
    when cust_name ilike 'Lassen%' THEN 'Lassen'
    when cust_name ilike 'Lazy Acres%' THEN 'Lazy Acres'
    when cust_name ilike 'Earth Fare%' THEN 'Earth Fare'

    END AS group_name, cust_name, cust_number,
 cust_id

FROM custinfo
 JOIN custtype ON cust_custtype_id = custtype_id
 LEFT JOIN cntct on cust_cntct_id = cntct_id
 JOIN addr on cntct_addr_id = addr_id

 where custtype_descrip ilike 'retail%'
 AND (cust_name ilike 'pcc%' or
  (cust_name ilike 'central market%'
  AND addr_state ilike 'TX') or
  cust_name ilike 'Akin''s Natural Foods%' OR
  cust_name ilike 'Chamberlin''s Natural Foods%' OR
  cust_name ilike '%earth%Origin%' OR
  cust_name ilike 'fresh thyme farmers market%' or
  cust_name ilike 'green acres%' or
  cust_name ilike '%market street #%' or
  cust_name ilike 'Mom''s Organic Market%' or
  cust_name ilike 'mother''s market & kitchen%' or
  cust_name ilike 'new leaf community market%' or
  cust_name ilike 'new seasons market%' or
  cust_name ilike 'pcc%' or
  cust_name ilike 'pharmaca%' or
  cust_name ilike 'sprouts farmers market%' or
  cust_name ilike 'vitamin cottage%' or
  (cust_name ilike 'whole foods market%' OR cust_name ilike '365%' or cust_name ilike 'UNFI Sales') or
  cust_name ilike 'Lassen%' or
  cust_name ilike 'Lazy Acres%' or
  cust_name ilike 'Earth Fare%' )

  GROUP BY
  group_name, cust_name, cust_number,
  cust_id
  ),

-----------------------------------------------------------------
--  Membership Key Account Entries NCG and INFRA by charass_value
--  Author: Diana Newcomb
-----------------------------------------------------------------

membership
(group_name, cust_name, cust_number,
cust_id)

AS (
SELECT DISTINCT

 CASE WHEN charass_value = 'INFRA' THEN 'INFRA'
     WHEN charass_value = 'NCG' THEN 'NCG' END
 as group_name, cust_name, cust_number,
 cust_id

FROM custinfo
join custtype on cust_custtype_id = custtype_id
join charass on cust_id = charass_target_id
join salesrep on cust_salesrep_id = salesrep_id

where
custtype_descrip ilike 'retail%'
and charass_value =  'NCG' or charass_value = 'INFRA'

GROUP BY
group_name, cust_name, cust_number,
cust_id

),

-----------------------------------------------------------------
-- FDM Key Accounts
-- Author: Ryan Manzer
-----------------------------------------------------------------
fdm (group_name, cust_name, cust_number, cust_id)
AS (
  SELECT
    CASE WHEN cust_name ILIKE '%walgreen%' THEN 'Walgreens'
         WHEN (cust_name ILIKE '%cashwise%' OR cust_name ILIKE '%coborn%') THEN 'Cashwise & Coborn'
         WHEN cust_name LIKE '%GNC%' THEN 'GNC'
         WHEN cust_name LIKE 'HEB %' THEN 'HEB'
         WHEN cust_name ILIKE '%hy-vee%' THEN 'Hy-Vee'
         WHEN cust_name ILIKE '%king soopers%' THEN 'King Soopers'
         WHEN cust_name ILIKE '%lunds & byerlys%' THEN 'Lunds & Byerlys'
         WHEN cust_name ILIKE '%publix%' THEN 'Publix'
         WHEN cust_name ILIKE 'wegman%' THEN 'Wegmans' END
    AS group_name,
    cust_name,
    cust_number,
    cust_id
  FROM custinfo
  INNER JOIN custtype ON cust_custtype_id = custtype_id
  WHERE custtype_descrip ILIKE 'us mass market'


  GROUP BY
  group_name,
  cust_name,
  cust_number,
  cust_id
),
-----------------------------------------------------------------
-- Amazon Customer Consolidation
-- Author: Ryan Manzer
-----------------------------------------------------------------
amz(group_name, cust_name, cust_number, cust_id)
AS (
  SELECT 
	CASE WHEN cust_name ILIKE 'Amazon - Retail (%' THEN 'Amazon - Retail' END
	AS group_name,
	cust_name,
	cust_number,
	cust_id
  FROM custinfo
  GROUP BY
	group_name,
	cust_name,
	cust_number,
	cust_id
),

----------------------------------------------------------------
-- iHerb Customer Consolidation
-- Author: Felina Smith (& Ryan Manzer)
----------------------------------------------------------------
iherb(group_name, cust_name, cust_number, cust_id)
AS (
  SELECT
	 CASE WHEN cust_name ILIKE '%iherb%' THEN 'iHerb' END
	AS group_name,
	cust_name,
	cust_number,
	cust_id
	FROM custinfo
	INNER JOIN custtype ON cust_custtype_id = custtype_id
	WHERE custtype_descrip ILIKE 'Export%'
	GROUP BY
	group_name,
	cust_name,
	cust_number,
	cust_id
),
---------------------------------------------------------
--  Pro Key Accounts
--  Author: Ryan Manzer (& Felina Smith)
---------------------------------------------------------
pro_accounts(group_name, cust_name, cust_number, cust_id)
AS (
 SELECT DISTINCT
 CASE WHEN cust_name ilike 'Emerson Ecologics%' THEN 'Emerson Ecologics'
 WHEN cust_name ilike 'Natural Partners%' THEN 'Natural Partners'
 WHEN cust_name ilike 'Pure Formulas%' THEN 'Pure Formulas'
 WHEN cust_name ilike 'RGH Enterprises%' THEN 'RGH Enterprises'
 WHEN cust_name ilike 'Supplement First%' THEN 'Supplement First'
 WHEN cust_name ilike 'Doctor''s Supplement Store%' THEN 'Doctor''s Supplement Store'
 WHEN cust_name ilike 'Village Green Apothecary%' THEN 'Village Green Apothecary'
 WHEN cust_name ilike 'Dr Pure Natural%' THEN 'Dr Pure Natural'
 WHEN cust_name ilike 'Lucky Vitamin%' THEN 'Lucky Vitamin'
 WHEN cust_name ilike 'Warrior Angels%' THEN 'Warrior Angels'
 WHEN cust_name ilike 'WholeScripts%' THEN 'WholeScripts/Xymogen'
 WHEN cust_name ilike 'Pure Prescriptions%' THEN 'Pure Prescriptions'
 WHEN cust_name ilike 'Hockert Sales%' THEN 'Hockert Sales'
 WHEN cust_name ilike 'Jorge L Serra%' THEN 'Jorge L Serra'
 WHEN cust_name ilike 'PetMed Express%' THEN 'PetMed Express'
 END AS group_name, cust_name, cust_number,
 cust_id
FROM custinfo
 JOIN custtype ON cust_custtype_id = custtype_id
 JOIN salesrep ON cust_salesrep_id = cust_salesrep_id
 LEFT JOIN cntct on cust_cntct_id = cntct_id
 JOIN addr on cntct_addr_id = addr_id
 where custtype_descrip ilike 'pro%'
 AND (cust_name ilike 'pcc%' or
  cust_name ilike 'Natural Partners%' OR
  cust_name ilike 'Emerson Ecologics%' OR
  cust_name ilike 'Pure Formulas%' OR
  cust_name ilike 'RGH Enterprises%' or
  cust_name ilike 'Supplement First%' or
  cust_name ilike 'Doctor''s Supplement Store%' or
  cust_name ilike 'Village Green Apothecary%' or
  cust_name ilike 'Dr Pure Natural%' or
  cust_name ilike 'Lucky Vitamin%' or
  cust_name ilike 'Warrior Angels%' or
  cust_name ilike 'WholeScripts/Xymogen%' or
  cust_name ilike 'Pure Prescriptions%' or
  cust_name ilike 'Hockert Sales%' or
  cust_name ilike 'Jorge L Serra%' or
  cust_name ilike 'PetMed Express%'
	  )
  GROUP BY
  group_name, cust_name, cust_number,
  cust_id
  ),
-----------------------------------------------------------------
--  UNIONS all key accounts
-----------------------------------------------------------------

keys
(group_name, cust_name, cust_number,
cust_id)

AS (SELECT * FROM simple
    UNION
    SELECT * FROM membership
    UNION
    SELECT * FROM fdm
	UNION
	SELECT * FROM amz
	UNION 
	SELECT * FROM iherb
	UNION
	SELECT * FROM pro_accounts
)


SELECT * FROM keys
WHERE group_name IS NOT NULL

GROUP BY
group_name, cust_name, cust_number,
cust_id

ORDER BY group_name
