--
-- 2nd attempt at YTD Query
-- C. Ryan Manzer
-- 10/16/2019
--

  SELECT
  ROUND(SUM(cohist_unitprice*cohist_qtyshipped),2) AS revenue,

  date_part('year', cohist_invcdate) AS year,

  date_part('quarter', cohist_invcdate) AS quarter,

  date_part('month', cohist_invcdate) AS month,

  date_part('doy', cohist_invcdate) AS doy

FROM cohist
INNER JOIN custinfo ON cohist_cust_id = cust_id
INNER JOIN custtype ON cust_custtype_id = custtype_id

WHERE cohist_invcdate >= (SELECT
DATE(to_char(EXTRACT(YEAR FROM CURRENT_DATE) -1, '9999') ||'-1-1'))
AND date_part('doy', cohist_invcdate) <= date_part('doy', CURRENT_DATE)

GROUP BY 2,3,4,5
ORDER BY 2,3,4,5
