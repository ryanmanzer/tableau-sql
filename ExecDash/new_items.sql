SELECT
item_number,
item_descrip1,
item_created
FROM item
WHERE item_created >= CURRENT_DATE - INTERVAL '12 months'
AND item_number SIMILAR TO '[A-Z]{3}-[0-9]{5}'
