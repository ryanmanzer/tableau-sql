SELECT
  date_part('year', cohist_invcdate) AS Year
-- , item_number SIMILAR TO '^\c{3}-\c{5}$' AS is_sku
, REGEXP_MATCHES(item_number,'^[A-Z]{3}-[0-9]{5}$') AS is_sku
, SUM(cohist_qtyshipped) AS Quantity
, SUM(ROUND(cohist_qtyshipped * cohist_unitprice, 2)) AS Revenue

FROM cohist
INNER JOIN itemsite ON cohist_itemsite_id = itemsite_id
INNER JOIN item ON itemsite_item_id = item_id
WHERE cohist_invcdate >= '2018-01-01'::DATE
AND cohist_unitprice <> 0
GROUP BY 1, 2
