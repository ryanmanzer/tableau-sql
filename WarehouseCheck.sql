SELECT

COUNT (DISTINCT cohead_number) AS "order_count",
MIN(cohead_created) AS "first_date",
MAX(cohead_created) AS "last_date"

-- ch.cohead_id AS "order_id",

-- ch.cohead_created AS "order_date",

-- ch.cohead_shipvia AS "shipped_by",

-- ch.cohead_shiptoname AS "ship_to",

-- ROUND(SUM(ci.coitem_qtyord),0) AS "order_qty",

-- ROUND(SUM(ci.coitem_qtyshipped),0) AS "shipped_qty",

-- ch.cohead_warehous_id AS "site",

-- ct.custtype_id AS "ct_id",

-- ct.custtype_code AS "cust_type"





FROM

cohead AS ch

LEFT JOIN coitem AS ci ON ci.coitem_cohead_id = ch.cohead_id

JOIN itemsite AS iis ON iis.itemsite_id = ci.coitem_itemsite_id

JOIN item as i ON i.item_id = iis.itemsite_item_id

JOIN whsinfo AS wh ON wh.warehous_id = ch.cohead_warehous_id

JOIN custinfo AS c ON c.cust_id = ch.cohead_cust_id

JOIN custtype as ct ON ct.custtype_id = c.cust_custtype_id



WHERE

--ci.coitem_status = 'O' --Commented out the status identifier of "Open" C = closed

--AND

ch.cohead_created BETWEEN '2020-02-24'::DATE AND '2020-03-24'::DATE

AND i.item_number NOT LIKE '%MKT%'



-- GROUP BY

-- "order_number",

-- "order_id",

-- "order_date",

-- "shipped_by",

-- "ship_to",

-- "site",

-- "ct_id",

-- "cust_type"
