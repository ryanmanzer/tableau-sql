SELECT
char_name
,charass_target_type
,charass_value
,cohist_ordernumber
,cohist_orderdate
,cohist_invcnumber
,cohist_invcdate
,cohist_qtyshipped
,cohist_unitprice
,cohist_unitcost
,ROUND(SUM(cohist_qtyshipped*cohist_unitprice),2) AS extended_price
,cohist_billtoname
,cohist_billtoaddress1
,cohist_billtoaddress2
,cohist_billtoaddress3
,cohist_biltocity
,cohist_billtostate
,cohist_shiptoname
,cohist_shiptoaddress1
,cohist_shiptoaddress2
,cohist_shiptoaddress3
,cohist_shiptocity
,cohist-shiptostate
,cust_name
,custtype_code
,item_number
,item_descrip1
,salesrep_name

FROM cohist
INNER JOIN custinfo ON cohist_cust_id = cust_id
INNER JOIN custtype ON cust_custtype_id = custtype_id
