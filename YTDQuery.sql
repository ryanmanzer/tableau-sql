--
--  YTD QTD MTD Query
--  C. Ryan Manzer
--  10/15/2019
--

WITH YTD AS (
  SELECT
  '1' AS key,
  ROUND(SUM(cohist_unitprice * cohist_qtyshipped),2) AS current_ytd
  FROM cohist
  WHERE EXTRACT(YEAR FROM cohist_invcdate)  = EXTRACT(YEAR FROM CURRENT_DATE)
)

-- , PYTD AS (
--   SELECT
--   '1' AS key,
--   ROUND(SUM(cohist_unitprice * cohist_qtyshipped),2) previous_ytd
--   FROM cohist
--   WHERE date_part('year', cohist_invcdate) = (date_part('year', CURRENT_DATE) -1) AND
--   EXTRACT(DOY FROM cohist_invcdate) <= EXTRACT(DOY FROM CURRENT_DATE)
-- )
SELECT
  YTD.current_ytd
--   , PYTD.previous_ytd
  FROM YTD
--   INNER JOIN PYTD ON YTD.key = PYTD.key
