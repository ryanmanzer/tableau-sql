--FORECASTING -- Totals and units shipped
--REPLACE DATES


---------------------------------------------
--             ALL OTHERS
---------------------------------------------
(
SELECT

CAST ('All Other Accounts' AS TEXT) as Groupname,
cust_name,
cust_number,
CASE WHEN cust_name ilike '%employee%'
	THEN 'True'
	ELSE 'False'
	END
	AS Employee_test,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1,
sum ( round ( cohist_qtyshipped, 2 )) as units,
sum (round (cohist_unitprice * cohist_qtyshipped,2 )) as total

from cohist

left join itemsite on cohist_itemsite_id = itemsite_id
left join item on itemsite_item_id = item_id
left join custinfo on cohist_cust_id = cust_id
join custtype on cust_custtype_id = custtype_id
join salesrep on cust_salesrep_id = salesrep_id

where cust_name not ilike 'pcc%'
and cust_name not ilike 'central market%'
and cust_name not ilike 'fresh thyme farmers market%'
and cust_name not ilike 'Mom''s Organic Market%'
and cust_name not ilike 'mother''s market & kitchen%'
and cust_name not ilike 'new leaf community market%'
and cust_name not ilike 'new seasons market%'
and cust_name not ilike 'pharmaca%'
and cust_name not ilike 'sprouts farmers market%'
and cust_name not ilike 'vitamin cottage%'
and cust_name not ilike 'whole foods market%'
and cust_name not ilike '365%'
and salesrep_name not ilike 'unfi sales%'
--and cust_name not ilike '%employee%'

and custtype_descrip ilike 'retail%'
and (item_number ilike 'NUS%' or item_number ilike 'RUS%')
and cohist_invcdate BETWEEN '01/01/2016' AND CURRENT_DATE

GROUP BY

cust_name,
cust_number,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1

EXCEPT

(SELECT

CAST ('All Other Accounts' AS TEXT) as Groupname,
cust_name,
cust_number,
 CASE WHEN cust_name ilike '%employee%'
	THEN 'True'
	ELSE 'False'
	END
	AS Employee_test,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1,
sum ( round ( cohist_qtyshipped, 2 )) as units,
sum (round (cohist_unitprice * cohist_qtyshipped,2 )) as total

from cohist

left join itemsite on cohist_itemsite_id = itemsite_id
left join item on itemsite_item_id = item_id
left join custinfo on cohist_cust_id = cust_id
join custtype on cust_custtype_id = custtype_id
join salesrep on cust_salesrep_id = salesrep_id
left join charass on charass_target_id = cust_id

where custtype_descrip ilike 'retail%'
and (item_number ilike 'NUS%' or item_number ilike 'RUS%')
and (charass_value = 'INFRA')
and cohist_invcdate BETWEEN '01/01/2016' AND CURRENT_DATE

GROUP BY
cust_name,
cust_number,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1

UNION

SELECT

CAST ('All Other Accounts' AS TEXT) as Groupname,
cust_name,
cust_number,
CASE WHEN cust_name ilike '%employee%'
	THEN 'True'
	ELSE 'False'
	END
	AS Employee_test,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1,
sum ( round ( cohist_qtyshipped, 2 )) as units,
sum (round (cohist_unitprice * cohist_qtyshipped,2 )) as total

from cohist

left join itemsite on cohist_itemsite_id = itemsite_id
left join item on itemsite_item_id = item_id
left join custinfo on cohist_cust_id = cust_id
join custtype on cust_custtype_id = custtype_id
join salesrep on cust_salesrep_id = salesrep_id
left join charass on charass_target_id = cust_id

where cohist_invcdate BETWEEN '01/01/2016' AND CURRENT_DATE
and custtype_descrip ilike 'retail%'
and (item_number ilike 'NUS%' or item_number ilike 'RUS%')
and (charass_value = 'NCG')

GROUP BY
cust_name,
cust_number,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1)
)

UNION

--------------------------------------
--           KEY ACCOUNTS
--           PLUS NCG AND
--           INFRA
--------------------------------------

(SELECT

CAST ('Central Market - TX' AS TEXT) as Groupname,
cust_name,
cust_number,
CASE WHEN cust_name ilike '%employee%'
	THEN 'True'
	ELSE 'False'
	END
	AS Employee_test,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1,
sum ( round ( cohist_qtyshipped, 2 )) as units,
sum (round (cohist_unitprice * cohist_qtyshipped,2 )) as total

from cohist

left join itemsite on cohist_itemsite_id = itemsite_id
left join item on itemsite_item_id = item_id
left join custinfo on cohist_cust_id = cust_id
join custtype on cust_custtype_id = custtype_id
left join cntct on cust_cntct_id = cntct_id
join addr on cntct_addr_id = addr_id
join salesrep on cohist_salesrep_id = salesrep_id

where cust_name ilike 'central market%'
and cohist_invcdate BETWEEN '01/01/2016' AND CURRENT_DATE
and addr_state ilike 'TX'
and custtype_descrip ilike 'retail%'
and (item_number ilike 'NUS%' or item_number ilike 'RUS%')

GROUP BY
cust_name,
cust_number,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1

UNION

SELECT

CAST ('Fresh Thyme' AS TEXT) as Groupname,
cust_name,
cust_number,
CASE WHEN cust_name ilike '%employee%'
	THEN 'True'
	ELSE 'False'
	END
	AS Employee_test,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1,
sum ( round ( cohist_qtyshipped, 2 )) as units,
sum (round (cohist_unitprice * cohist_qtyshipped,2 )) as total

from cohist

left join itemsite on cohist_itemsite_id = itemsite_id
left join item on itemsite_item_id = item_id
left join custinfo on cohist_cust_id = cust_id
join custtype on cust_custtype_id = custtype_id
join salesrep on cohist_salesrep_id = salesrep_id

where cust_name ilike 'fresh thyme farmers market%'
and cohist_invcdate BETWEEN '01/01/2016' AND CURRENT_DATE
and custtype_descrip ilike 'retail%'
and (item_number ilike 'NUS%' or item_number ilike 'RUS%')

GROUP BY
cust_name,
cust_number,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1

UNION

SELECT

CAST ('INFRA' AS TEXT) as Groupname,
cust_name,
cust_number,
CASE WHEN cust_name ilike '%employee%'
	THEN 'True'
	ELSE 'False'
	END
	AS Employee_test,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1,
sum ( round ( cohist_qtyshipped, 2 )) as units,
sum (round (cohist_unitprice * cohist_qtyshipped,2 )) as total

from cohist

left join itemsite on cohist_itemsite_id = itemsite_id
left join item on itemsite_item_id = item_id
left join custinfo on cohist_cust_id = cust_id
join custtype on cust_custtype_id = custtype_id
join charass c1 on cust_id = c1.charass_target_id
join salesrep on cohist_salesrep_id = salesrep_id

where cohist_invcdate BETWEEN '01/01/2016' AND CURRENT_DATE
and c1.charass_value = 'INFRA'
and custtype_descrip ilike 'retail%'
and (item_number ilike 'NUS%' or item_number ilike 'RUS%')

GROUP BY
cust_name,
cust_number,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1

UNION

SELECT

CAST ('Mom''s Market' AS TEXT) as Groupname,
cust_name,
cust_number,
CASE WHEN cust_name ilike '%employee%'
	THEN 'True'
	ELSE 'False'
	END
	AS Employee_test,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1,
sum ( round ( cohist_qtyshipped, 2 )) as units,
sum (round (cohist_unitprice * cohist_qtyshipped,2 )) as total

from cohist

left join itemsite on cohist_itemsite_id = itemsite_id
left join item on itemsite_item_id = item_id
left join custinfo on cohist_cust_id = cust_id
join custtype on cust_custtype_id = custtype_id
join salesrep on cohist_salesrep_id = salesrep_id

where cust_name ilike 'Mom''s Organic Market%'
and cohist_invcdate BETWEEN '01/01/2016' AND CURRENT_DATE
and custtype_descrip ilike 'retail%'
and (item_number ilike 'NUS%' or item_number ilike 'RUS%')

GROUP BY
cust_name,
cust_number,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1

UNION

SELECT

CAST ('Mom''s Market' AS TEXT) as Groupname,
cust_name,
cust_number,
CASE WHEN cust_name ilike '%employee%'
	THEN 'True'
	ELSE 'False'
	END
	AS Employee_test,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1,
sum ( round ( cohist_qtyshipped, 2 )) as units,
sum (round (cohist_unitprice * cohist_qtyshipped,2 )) as total

from cohist

left join itemsite on cohist_itemsite_id = itemsite_id
left join item on itemsite_item_id = item_id
left join custinfo on cohist_cust_id = cust_id
join custtype on cust_custtype_id = custtype_id
join salesrep on cohist_salesrep_id = salesrep_id

where  cust_name ilike 'mother''s market & kitchen%'
and cohist_invcdate BETWEEN '01/01/2016' AND CURRENT_DATE
and custtype_descrip ilike 'retail%'
and (item_number ilike 'NUS%' or item_number ilike 'RUS%')

GROUP BY
cust_name,
cust_number,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1

UNION

SELECT

CAST ('NCG' AS TEXT) as Groupname,
cust_name,
cust_number,
CASE WHEN cust_name ilike '%employee%'
	THEN 'True'
	ELSE 'False'
	END
	AS Employee_test,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1,
sum ( round ( cohist_qtyshipped, 2 )) as units,
sum (round (cohist_unitprice * cohist_qtyshipped,2 )) as total

from custinfo

join cohist on cohist_cust_id = cust_id
left join cntct on cust_cntct_id = cntct_id
left join addr on cntct_addr_id = addr_id
join charass c1 on cust_id = c1.charass_target_id
left join itemsite on itemsite_id = cohist_itemsite_id
left join item on item_id = itemsite_item_id
JOIN custtype on cust_custtype_id = custtype_id
join salesrep on cohist_salesrep_id = salesrep_id

where cohist_invcdate BETWEEN '01/01/2016' AND CURRENT_DATE
and c1.charass_value = 'NCG'
and custtype_descrip ilike 'retail%'
and (item_number ilike 'NUS%' or item_number ilike 'RUS%')

GROUP BY
cust_name,
cust_number,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1

UNION

SELECT

CAST ('New Seasons - New Leaf' AS TEXT) as Groupname,
cust_name,
cust_number,
CASE WHEN cust_name ilike '%employee%'
	THEN 'True'
	ELSE 'False'
	END
	AS Employee_test,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1,
sum ( round ( cohist_qtyshipped, 2 )) as units,
sum (round (cohist_unitprice * cohist_qtyshipped,2 )) as total

from cohist

left join itemsite on cohist_itemsite_id = itemsite_id
left join item on itemsite_item_id = item_id
left join custinfo on cohist_cust_id = cust_id
join custtype on cust_custtype_id = custtype_id
join salesrep on cohist_salesrep_id = salesrep_id

where (cust_name ilike 'new leaf community market%' or cust_name ilike 'new seasons market%')
and cohist_invcdate BETWEEN '01/01/2016' AND CURRENT_DATE
and custtype_descrip ilike 'retail%'
and (item_number ilike 'NUS%' or item_number ilike 'RUS%')

GROUP BY
cust_name,
cust_number,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1

UNION

SELECT

CAST ('PCC' AS TEXT) as Groupname,
cust_name,
cust_number,
CASE WHEN cust_name ilike '%employee%'
	THEN 'True'
	ELSE 'False'
	END
	AS Employee_test,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1,
sum ( round ( cohist_qtyshipped, 2 )) as units,
sum (round (cohist_unitprice * cohist_qtyshipped,2 )) as total

from cohist

left join itemsite on cohist_itemsite_id = itemsite_id
left join item on itemsite_item_id = item_id
left join custinfo on cohist_cust_id = cust_id
join custtype on cust_custtype_id = custtype_id
join salesrep on cohist_salesrep_id = salesrep_id

where cust_name ilike 'pcc%'
and cohist_invcdate BETWEEN '01/01/2016' AND CURRENT_DATE

and custtype_descrip ilike 'retail%'
and (item_number ilike 'NUS%' or item_number ilike 'RUS%')

GROUP BY
cust_name,
cust_number,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1

UNION

SELECT

CAST ('Pharmaca' AS TEXT) as Groupname,
cust_name,
cust_number,
CASE WHEN cust_name ilike '%employee%'
	THEN 'True'
	ELSE 'False'
	END
	AS Employee_test,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1,
sum ( round ( cohist_qtyshipped, 2 )) as units,
sum (round (cohist_unitprice * cohist_qtyshipped,2 )) as total

from cohist

left join itemsite on cohist_itemsite_id = itemsite_id
left join item on itemsite_item_id = item_id
left join custinfo on cohist_cust_id = cust_id
join custtype on cust_custtype_id = custtype_id
join salesrep on cohist_salesrep_id = salesrep_id

where cust_name ilike 'pharmaca%'
and cohist_invcdate BETWEEN '01/01/2016' AND CURRENT_DATE
and custtype_descrip ilike 'retail%'
and (item_number ilike 'NUS%' or item_number ilike 'RUS%')

GROUP BY
cust_name,
cust_number,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1

UNION

SELECT

CAST ('Sprouts' AS TEXT) as Groupname,
cust_name,
cust_number,
CASE WHEN cust_name ilike '%employee%'
	THEN 'True'
	ELSE 'False'
	END
	AS Employee_test,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1,
sum ( round ( cohist_qtyshipped, 2 )) as units,
sum (round (cohist_unitprice * cohist_qtyshipped,2 )) as total

from cohist

left join itemsite on cohist_itemsite_id = itemsite_id
left join item on itemsite_item_id = item_id
left join custinfo on cohist_cust_id = cust_id
join custtype on cust_custtype_id = custtype_id
join salesrep on cohist_salesrep_id = salesrep_id

where cust_name ilike 'sprouts farmers market%'
and cohist_invcdate BETWEEN '01/01/2016' AND CURRENT_DATE
and custtype_descrip ilike 'retail%'
and (item_number ilike 'NUS%' or item_number ilike 'RUS%')

GROUP BY
cust_name,
cust_number,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1

UNION

SELECT

CAST ('Vitamin Cottage' AS TEXT) as Groupname,
cust_name,
cust_number,
CASE WHEN cust_name ilike '%employee%'
	THEN 'True'
	ELSE 'False'
	END
	AS Employee_test,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1,
sum ( round ( cohist_qtyshipped, 2 )) as units,
sum (round (cohist_unitprice * cohist_qtyshipped,2 )) as total

from cohist

left join itemsite on cohist_itemsite_id = itemsite_id
left join item on itemsite_item_id = item_id
left join custinfo on cohist_cust_id = cust_id
join custtype on cust_custtype_id = custtype_id
join salesrep on cohist_salesrep_id = salesrep_id

where cust_name ilike 'vitamin cottage%'
and cohist_invcdate BETWEEN '01/01/2016' AND CURRENT_DATE
and custtype_descrip ilike 'retail%'
and (item_number ilike 'NUS%' or item_number ilike 'RUS%')

GROUP BY
cust_name,
cust_number,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1

UNION

SELECT

CAST ('UNFI' AS TEXT) as Groupname,
cust_name,
cust_number,
CASE WHEN cust_name ilike '%employee%'
	THEN 'True'
	ELSE 'False'
	END
	AS Employee_test,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1,
sum ( round ( cohist_qtyshipped, 2 )) as units,
sum (round (cohist_unitprice * cohist_qtyshipped,2 )) as total

from cohist

left join itemsite on cohist_itemsite_id = itemsite_id
left join item on itemsite_item_id = item_id
left join custinfo on cohist_cust_id = cust_id
join custtype on cust_custtype_id = custtype_id
join salesrep on cohist_salesrep_id = salesrep_id

where (cust_name ilike 'whole foods market%' or cust_name ilike '365%' or salesrep_name ilike 'unfi sales%')
and cohist_invcdate BETWEEN '01/01/2016' AND CURRENT_DATE
and custtype_descrip ilike 'retail%'
and (item_number ilike 'NUS%' or item_number ilike 'RUS%')

GROUP BY
cust_name,
cust_number,
salesrep_name,
cohist_invcnumber,
cohist_invcdate,
cohist_unitprice,
cohist_shiptocity,
cohist_shiptostate,
cohist_shiptozip,
cust_dateadded,
custtype_descrip,
item_number,
item_descrip1

)
