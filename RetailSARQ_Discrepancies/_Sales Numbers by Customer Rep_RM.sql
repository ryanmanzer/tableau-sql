DROP TABLE IF EXISTS DATES;
CREATE TEMPORARY TABLE DATES AS

SELECT
'01/01/2020'::DATE AS "START DATE",
current_date - 1 AS "END DATE"
--'12/31/2018'::DATE as "END DATE"
;

DROP TABLE IF EXISTS CUST_BASED_SALES;
CREATE TEMPORARY TABLE CUST_BASED_SALES AS

SELECT
salesrep_name COLLATE "C" as "Rep",
SUM(round(cohist.cohist_qtyshipped*cohist.cohist_unitprice,2))AS "Sales Total"

from cohist

LEFT JOIN itemsite on itemsite_id = cohist_itemsite_id
LEFT JOIN item on item_id = itemsite_item_id
LEFT JOIN charass on charass_target_id = item_id
LEFT JOIN char on char_id = charass_char_id
JOIN custinfo on cohist_cust_id = cust_id
JOIN salesrep on salesrep_id = cust_salesrep_id
JOIN custtype on cust_custtype_id = custtype_id

where cohist.cohist_invcdate >= (SELECT "START DATE" FROM DATES)  AND cohist.cohist_invcdate <= (SELECT "END DATE" FROM DATES)
and char.char_name = 'IsCommissioned'
and charass.charass_value ilike 'TRUE'
and charass.charass_target_type = 'I'

and (custtype_descrip ilike '%retail%' or custtype_descrip ilike '%mass%' or cust_name ilike 'avafina pet%')
and salesrep_name not ilike 'retail rep'
and salesrep_name not ilike 'joar%'

GROUP BY "Rep"

UNION

SELECT

'z_Grand_Total' as "Rep",
SUM(round(cohist.cohist_qtyshipped*cohist.cohist_unitprice,2))AS "Sales Total"

from cohist

LEFT JOIN itemsite on itemsite_id = cohist_itemsite_id
LEFT JOIN item on item_id = itemsite_item_id
LEFT JOIN charass on charass_target_id = item_id
LEFT JOIN char on char_id = charass_char_id
JOIN custinfo on cohist_cust_id = cust_id
JOIN salesrep on salesrep_id = cust_salesrep_id
JOIN custtype on cust_custtype_id = custtype_id

where cohist.cohist_invcdate >= (SELECT "START DATE" FROM DATES)  AND cohist.cohist_invcdate <= (SELECT "END DATE" FROM DATES)
and char.char_name = 'IsCommissioned'  -- 1. The resrtiction to Comissioned sales takes place across three clauses
and charass.charass_value ilike 'TRUE' -- 2.
and charass.charass_target_type = 'I' -- 3.

and (custtype_descrip ilike '%retail%' or custtype_descrip ilike '%mass%' or cust_name ilike 'avafina pet%')  -- Odd customer type and name
and salesrep_name not ilike 'retail rep' -- And exclusion her
and salesrep_name not ilike 'joar%'

GROUP BY "Rep";

WITH NON_COMMISSIONED_SALES AS (
  SELECT
  salesrep_name AS "Rep",
  SUM(ROUND(cohist_qtyshipped * cohist_unitprice, 2)) AS "NC Sales"
  FROM cohist
  LEFT JOIN itemsite on itemsite_id = cohist_itemsite_id
  LEFT JOIN item on item_id = itemsite_item_id
  JOIN custinfo on cohist_cust_id = cust_id
  JOIN salesrep on salesrep_id = cust_salesrep_id
  JOIN custtype on cust_custtype_id = custtype_id
  WHERE cohist_invcdate BETWEEN (SELECT "START DATE" FROM DATES) AND (SELECT "END DATE" FROM DATES)
  and (custtype_descrip ilike '%retail%' or custtype_descrip ilike '%mass%' or cust_name ilike 'avafina pet%')  -- Odd customer type and name
  and salesrep_name not ilike 'retail rep' -- And exclusion her
  and salesrep_name not ilike 'joar%'
  GROUP BY "Rep"

  UNION

  SELECT
  'z_Grand_Total' AS "Rep",
  SUM(ROUND(cohist_qtyshipped * cohist_unitprice, 2)) AS "NC Sales"
  FROM cohist
  LEFT JOIN itemsite on itemsite_id = cohist_itemsite_id
  LEFT JOIN item on item_id = itemsite_item_id
  JOIN custinfo on cohist_cust_id = cust_id
  JOIN salesrep on salesrep_id = cust_salesrep_id
  JOIN custtype on cust_custtype_id = custtype_id
  WHERE cohist_invcdate BETWEEN (SELECT "START DATE" FROM DATES) AND (SELECT "END DATE" FROM DATES)
  and (custtype_descrip ilike '%retail%' or custtype_descrip ilike '%mass%' or cust_name ilike 'avafina pet%')  -- Odd customer type and name
  and salesrep_name not ilike 'retail rep' -- And exclusion her
  and salesrep_name not ilike 'joar%'
  GROUP BY "Rep"

), ALL_REP_SALES AS (
  SELECT
  salesrep_name COLLATE "C" as "Rep",
  SUM(round(cohist.cohist_qtyshipped*cohist.cohist_unitprice,2))AS "AR Total"
  from cohist
  LEFT JOIN itemsite on itemsite_id = cohist_itemsite_id
  LEFT JOIN item on item_id = itemsite_item_id
  LEFT JOIN charass on charass_target_id = item_id
  LEFT JOIN char on char_id = charass_char_id
  JOIN custinfo on cohist_cust_id = cust_id
  JOIN salesrep on salesrep_id = cust_salesrep_id
  JOIN custtype on cust_custtype_id = custtype_id
  where cohist.cohist_invcdate BETWEEN (SELECT "START DATE" FROM DATES)  AND (SELECT "END DATE" FROM DATES)
  and char.char_name = 'IsCommissioned'
  and charass.charass_value ilike 'TRUE'
  and charass.charass_target_type = 'I'
  and (custtype_descrip ilike '%retail%' or custtype_descrip ilike '%mass%' or cust_name ilike 'avafina pet%')
  GROUP BY "Rep"

  UNION

  SELECT
  'z_Grand_Total' as "Rep",
  SUM(round(cohist.cohist_qtyshipped*cohist.cohist_unitprice,2))AS "AR Total"
  from cohist
  LEFT JOIN itemsite on itemsite_id = cohist_itemsite_id
  LEFT JOIN item on item_id = itemsite_item_id
  LEFT JOIN charass on charass_target_id = item_id
  LEFT JOIN char on char_id = charass_char_id
  JOIN custinfo on cohist_cust_id = cust_id
  JOIN salesrep on salesrep_id = cust_salesrep_id
  JOIN custtype on cust_custtype_id = custtype_id
  where cohist.cohist_invcdate BETWEEN (SELECT "START DATE" FROM DATES)  AND (SELECT "END DATE" FROM DATES)
  and char.char_name = 'IsCommissioned'  -- 1. The resrtiction to Comissioned sales takes place across three clauses
  and charass.charass_value ilike 'TRUE' -- 2.
  and charass.charass_target_type = 'I' -- 3.
  and (custtype_descrip ilike '%retail%' or custtype_descrip ilike '%mass%' or cust_name ilike 'avafina pet%')
  GROUP BY "Rep"
)
SELECT
A."Rep",
A."Sales Total",
B."NC Sales",
B."NC Sales" - A."Sales Total" AS "NC Diff",
(B."NC Sales" - A."Sales Total") / ABS(A."Sales Total") AS "% NC Diff",
C."AR Total",
C."AR Total" - A."Sales Total" AS "AR Diff",
(C."AR Total" - A."Sales Total") / ABS(A."Sales Total") AS "% AR Diff"
FROM CUST_BASED_SALES AS A
INNER JOIN NON_COMMISSIONED_SALES AS B ON A."Rep" = B."Rep"
INNER JOIN ALL_REP_SALES AS C ON B."Rep" = C."Rep"
