--
-- Key Account Mapping Table
-- This SQL query uses Common Table Expressions to generate a mapping between
-- cust_id and Key Account strings
-- by Ryan Manzer
--

--
-- Assigning Central Market - TX group
--

WITH central_texas AS (
  SELECT
  CAST ('Central Market - TX' AS TEXT) as group_name,
  cust_id
  from custinfo
  join custtype on cust_custtype_id = custtype_id
  left join cntct on cust_cntct_id = cntct_id
  join addr on cntct_addr_id = addr_id

  where cust_name ilike 'central market%'
  and addr_state ilike 'TX'
  and custtype_descrip ilike 'retail%'
),

--
-- Assigning Fresh Thyme Group
--

fresh_time AS (
  SELECT

  CAST ('Fresh Thyme' AS TEXT) as group_name,
  cust_id

  from custinfo
  join custtype on cust_custtype_id = custtype_id

  where cust_name ilike 'fresh thyme farmers market%'
  and custtype_descrip ilike 'retail%'
)

--
-- Unioning results to generate
-- cust_id <=> group_name mapping
--

SELECT
cust_id, group_name FROM central_texas

UNION

SELECT cust_id, group_name FROM fresh_time
