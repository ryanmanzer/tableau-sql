SELECT
cohist_ordernumber AS "Order Number",
cohist_invcdate AS "Invoice Date",
cohist_unitprice AS "Unit Price",
cohist_qtyshipped AS "Quantity Shipped",
cohist_shipdate AS "Ship Date",
cohist_orderdate AS "Order Date",
cohist_billtocity AS "Bill To City",
cohist_billtostate AS "Bill to State",
cohist_billtozip AS "Bill to Postal Code",
cohist_shiptocity AS "Ship To City",
cohist_shiptostate AS "Ship To State",
cohist_shiptozip AS "Ship To Postal code",
cust_number AS "Customer Number",
cust_name AS "Customer Name",
custtype_code AS "Customer Code",
item_number AS "Item Number",
item_descrip1 AS "Item Description",
salesrep_name AS "Sales Rep Name"

FROM cohist  -- Invoiced Line Item
INNER JOIN custinfo ON cohist_cust_id = cust_id  -- Customer master data
INNER JOIN custtype ON cust_custtype_id = custtype_id -- Customer category data
INNER JOIN itemsite ON cohist_itemsite_id = itemsite_id -- item storage (warehouse)
INNER JOIN item ON itemsite_item_id = item_id -- Item master data
INNER JOIN salesrep ON cohist_salesrep_id = salesrep_id -- sales office
WHERE cohist_invcdate > CURRENT_DATE - INTERVAL '2 years'  -- getting recent data
ORDER BY cohist_invcdate
