--
-- TOTAL BARE BONES REVENUE
--

SELECT
  SUM(ROUND(cohist_unitprice*cohist_qtyshipped,2)) AS revenue
FROM cohist
WHERE cohist_invcdate >= (SELECT
DATE(to_char(EXTRACT(YEAR FROM CURRENT_DATE) -1, '9999') ||'-1-1'))

RETURNS revenue: "315261375.16"

--
-- ADDING custinfo and custtype
--

SELECT
  SUM(ROUND(cohist_unitprice*cohist_qtyshipped,2)) AS revenue
FROM cohist
INNER JOIN custinfo ON cohist_cust_id = cust_id
INNER JOIN custtype ON cust_custtype_id = custtype_id


WHERE cohist_invcdate >= (SELECT
DATE(to_char(EXTRACT(YEAR FROM CURRENT_DATE) -1, '9999') ||'-1-1'))

RETURNS revenue: "315261261.00"

--
--  ADDING itemsite, item
--

SELECT
  SUM(ROUND(cohist_unitprice*cohist_qtyshipped,2)) AS revenue

FROM cohist
INNER JOIN custinfo ON cohist_cust_id = cust_id
INNER JOIN custtype ON cust_custtype_id = custtype_id
INNER JOIN itemsite ON cohist_itemsite_id = itemsite_id
INNER JOIN item ON itemsite_item_id = item_id

WHERE cohist_invcdate >= (SELECT
DATE(to_char(EXTRACT(YEAR FROM CURRENT_DATE) -1, '9999') ||'-1-1'))

RETURNS revenue: "329512071.92"
