SELECT
cohead_number,
coitem_linenumber,
coitem_status

FROM coitem
INNER JOIN cohead ON cohead_id = coitem_cohead_id

WHERE
(cohead_number, coitem_linenumber) IN
(
  ('1905791', '003'),
  ('1909464', '001'),
  ('1913872', '011'),
  ('1924062', '001'),
  ('1929164', '004')
)
